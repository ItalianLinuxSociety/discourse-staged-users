#!/bin/bash
 
cp /var/discourse/containers/app.yml app.yml
docker stop staged-users-new
docker rm staged-users-new
docker rmi staged-users
docker image build -t staged-users:latest .
docker run -d --name staged-users-new --env-file=.env -p 13336:13336 staged-users:latest
