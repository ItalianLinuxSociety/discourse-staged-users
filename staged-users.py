#!/usr/bin/env python

from flask import Flask, request, Response
from email.mime.text import MIMEText
import logging
import pydiscourse
import smtplib
import yaml
import os

logging.basicConfig(level=logging.INFO)
app = Flask(__name__)
with open(r'app.yml') as file:
    discourse_settings = yaml.load(file, Loader=yaml.FullLoader)
    smtp_server = discourse_settings['env']['DISCOURSE_SMTP_ADDRESS']
    sender_email = discourse_settings['env']['DISCOURSE_SMTP_USER_NAME']
    password = discourse_settings['env']['DISCOURSE_SMTP_PASSWORD']

# Define the allowed IP address
allowed_ip = "90.147.189.104"


@app.route('/webhook', methods=['POST'])
def respond():
    global smtp_server, sender_email, password
    # Get the client's IP address
    client_ip = request.remote_addr

    # Check if the client's IP address is allowed
    if client_ip != allowed_ip:
        app.logger.info('Request not allowed' + client_ip)
        return Response("Forbidden", status=403)

    data = request.json
    if data is not None and 'post' in data:
        username = os.environ.get('FLASK_DISCOURSE_USER')
        key = os.environ.get('FLASK_DISCOURSE_KEY')
        domain = os.environ.get('FLASK_DISCOURSE_DOMAIN')

        client = pydiscourse.DiscourseClient(
            (domain),
            api_username=(username),
            api_key=(key),
        )

        app.logger.info('Request from Discourse by {}'.format(data['post']['username']))
        user_info = client.user(data['post']['username'])
        if user_info['staged'] is True:
            print('Detected staged users')
            app.logger.info('Detected staged users')
            port = 587
            server = smtplib.SMTP(smtp_server, port)
            server.starttls()

            message = os.environ.get('FLASK_DISCOURSE_MAIL')
            subject = os.environ.get('FLASK_DISCOURSE_SUBJECT')

            if data['post']['post_number'] != 1:
                message = os.environ.get('FLASK_DISCOURSE_MAIL_REPLY')
                subject = os.environ.get('FLASK_DISCOURSE_SUBJECT_REPLY')

            message = message.format(domain, data['post']['topic_slug'], str(data['post']['topic_id']), data['post']['topic_title'], data['post']['cooked'])
            subject = subject.format(data['post']['topic_title'])

            msg = MIMEText(message, 'html')
            msg["Subject"] = subject
            msg["From"] = sender_email
            msg["To"] = user_info['email']
            try:
                server.login(sender_email, password)
                server.sendmail(sender_email, msg['To'], msg.as_string())
                print('Email sent')
                app.logger.info('Email sent')
            finally:
                server.quit()

            print('Request finished')
        
    return Response(status=200)


if __name__ == "__main__":
    app.run(host=os.environ.get('FLASK_RUN_HOST'), port=13336)
