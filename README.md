# Discourse Staged Users

The **Discourse Staged Users** Docker container is designed to send email notifications for Discourse "staged users", that are users without an account yet.
This script is particularly useful when users can create threads from email, so then they may want to know when there is a reply (but as default they don't get any reply).

This Docker container creates a web service that is designed to be called from Discourse itself, throught a webhook.

By @mte90, @Oiras0r

Co-maintainers: @valerio-bozzolan

## Installation

### 1. Generate Discourse API key

From Discourse, generate a new API key with Global permission from this page:

    /admin/api/keys

API Scopes:

- users.show - allowed parameters:
	- username
	- external_id
	- external_provider
- users.list

Copy the file `.env.example` to `.env`.

Put your new API key in the `.env` file under:

```
FLASK_DISCOURSE_KEY=
```

### 2. Release

Double-check your `.env` file. Set your custom messages.

On `deploy.sh` you can change the path to the Discourse yaml file that will be parsed from the script.  
That script is helpful also to restart a docker container.

The Docker Compose is EXPERIMENTAL and not completed.

## 
