# Use the official Python image
FROM python:3.8-slim-buster

# Set the working directory
WORKDIR /usr/src/app

# Set environment variables
ENV FLASK_APP=staged-users.py
ENV FLASK_RUN_HOST=0.0.0.0  

# Copy the necessary files
COPY staged-users.py staged-users.py
COPY app.yml app.yml

# Install dependencies
RUN pip install flask pydiscourse pyyaml

# Copy the entire project
# TODO: avoid this --bozz
COPY . .

# Set the entrypoint to run the Flask application
CMD ["python", "-m", "flask", "run", "-p", "13336"]

# Expose the port
EXPOSE 13336
